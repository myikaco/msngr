package msngr

import "github.com/go-redis/redis/v8"

type HandlerMatch struct {
	Matcher func(fieldName string) bool
	Handler func(msg redis.XMessage, output *interface{})
	Output  *interface{}
}

type CommandHandler struct {
	Command        string
	HandlerMatches []HandlerMatch
}

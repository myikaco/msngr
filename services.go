package msngr

import (
	"fmt"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
)

// ParseStream takes []redis.XStream and parses each message based on the IncomingMsgHandlers map.
func ParseStream(stream []redis.XStream, streamName, groupName, consumerName string, parserHandlers []CommandHandler) {
	if len(stream) > 0 && len(stream[0].Messages) > 0 && stream[0].Messages[0].ID != "0-0" {
		LoggerFunc(fmt.Sprintf("Parsing in stream %v as %v: \n%v", streamName, consumerName, stream[0].Messages))
	}

	//parse response
	if len(stream) <= 0 {
		fmt.Println("Stream to parse length is 0!")
		return
	}

	//for every stream (usually just one at a time)
	for _, strMsgs := range stream {
		//for every message in stream
		for _, m := range strMsgs.Messages {
			if m.ID != "0-0" && len(m.ID) > 8 {
				// fmt.Printf("Parsing new message: %v\n", m)

				//for every field-value pair in message
				for key, val := range m.Values {
					//find command handler that matches field
					var cmdHandler CommandHandler
					for _, ch := range parserHandlers {
						if ch.Command == key {
							cmdHandler = ch
						}
					}
					//loop through all matchers of command handler for the field
					for _, hm := range cmdHandler.HandlerMatches {
						//if matcher returns true, run associated handler
						if hm.Matcher(val.(string)) {
							hm.Handler(m, hm.Output)
						}
					}
				}

				//if group and consumer name filled, acknowledge msg
				if groupName != "" && consumerName != "" && len(m.ID) > 6 {
					AcknowledgeMsg(streamName, groupName, consumerName, m.ID)
				}
			}
		}
	}
}

// readAndParse takes a stream reader function <readFunc> and stream message parser function <parserFunc>.
// It runs <readFunc> to get new stream messages and passes the result to <parserFunc> for processing.
// First return: String which is either the lastID of the latest message read, or a message "OK" on successful claiming of a pending consumer group message.
// Second return: []redis.XStream streams and messages successfully read.
// Third return: Error (if any) in reading process.
func ReadAndParse(
	readFunc func(map[string]string, string) (interface{}, interface{}, error),
	readerLabel string,
	parserFunc func([]redis.XStream, string, string, string, []CommandHandler),
	args map[string]string,
	parserHandlers []CommandHandler) (string, []redis.XStream, error) {
	var ret string

	a, b, err := readFunc(args, readerLabel)

	if lastID, ok := a.(string); ok {
		parserFunc(b.([]redis.XStream), args["streamName"], args["groupName"], args["consumerName"], parserHandlers)
		return lastID, b.([]redis.XStream), err
	}
	if streams, ok := a.([]redis.XStream); ok {
		parserFunc(streams, args["streamName"], args["groupName"], args["consumerName"], parserHandlers)
		return "", streams, err
	}

	return ret, nil, err
}

// AutoClaimMsgsLoop consistently checks for and processes messages pending for more than <minIdle> milliseconds.
func AutoClaimMsgsLoop(listenStream, readerLabel, consGroup, cons, minIdle, startID, count string, parserHandlers []CommandHandler) {
	args := make(map[string]string)
	args["streamName"] = listenStream
	args["groupName"] = consGroup
	args["consumerName"] = cons
	args["start"] = startID
	args["count"] = count
	args["minIdleTime"] = minIdle

	for {
		// fmt.Println("\n" + colorYellow + "Autoclaim old pending msgs..." + colorReset)
		msg, _, err := ReadAndParse(AutoClaimPendingMsgs, readerLabel, ParseStream, args, parserHandlers)
		if err != nil {
			// fmt.Printf("%s \nSleeping 5 secs before retry", err.Error())
			time.Sleep(5000 * time.Millisecond)
		} else {
			if msg == "" {
				// fmt.Printf("<%v> No old pending msgs to autoclaim.\n", time.Now().Format("2006-01-02T15:04:05"))
			} else {
				// fmt.Printf("<%v> Autoclaim old pending msgs response: %v\n", time.Now().Format("2006-01-02T15:04:05"), msg)
			}
			// fmt.Println("Waiting 10 secs to retry...")
			time.Sleep(10000 * time.Millisecond)
		}
	}
}

// StreamListenLoop waits for and processes new messages from <listenStreamName>, until execStopCheck returns false to break loop.
// Uses consumer groups to ensure multiple instances using the same <consumerGroup> do NOT consume the same message more than once.
// Argument execStopCheck runs after each time receiving messages. Takes the following arguments: The streams that were just read and the error produced when reading them.
func StreamListenLoop(
	listenStreamName,
	readerLabel,
	lastRespID,
	consumerGroup,
	consumerID,
	count,
	lastIDSaveKey string,
	parserHandlers []CommandHandler,
	execStopCheck func([]redis.XStream, error) bool) {
	args := make(map[string]string)
	args["streamName"] = listenStreamName
	args["groupName"] = consumerGroup
	args["consumerName"] = consumerID
	args["start"] = lastRespID
	args["count"] = count
	for {
		// LoggerFunc(fmt.Sprintf("listening on stream(%v) | consumer = %v", listenStreamName, consumerID))

		newLastMsgID, readResMsgs, err := ReadAndParse(ReadStream, readerLabel, ParseStream, args, parserHandlers)
		if err != nil {
			LoggerFunc(fmt.Sprintf("%s Sleeping 5 secs before retry", err.Error()))
			time.Sleep(5000 * time.Millisecond)
		} else {
			//check if should continue listening loop
			doContinue := execStopCheck(readResMsgs, err)

			if !doContinue {
				LoggerFunc(fmt.Sprintf("BREAK listen loop(%v) | consumer = %v", listenStreamName, consumerID))
				break
			} else {
				args["start"] = newLastMsgID
				saveErr := SaveLastID(lastIDSaveKey, newLastMsgID)
				if saveErr != nil {
					LoggerFunc(fmt.Sprintf(saveErr.Error()))
				}
			}
		}
	}
}

func ListenConsecResponses(
	args map[string]interface{},
	readerLabel string,
	consecHeaderProcessor func(int, string, redis.XMessage, bool)) (interface{}, error) {
	//listen for first resp from order-svc with CONSEC_RESP field
	consecRespHeaders := []string{}
	consecRespListenArgs := make(map[string]string)
	consecRespListenArgs["streamName"] = args["botStream"].(string)
	consecRespListenArgs["groupName"] = args["consumerGroup"].(string)
	consecRespListenArgs["consumerName"] = args["consumerID"].(string)
	consecRespListenArgs["start"] = ">"
	consecRespListenArgs["count"] = "1"
	var interConsecRespHeaders interface{}
	var retErr error
	for {
		if len(consecRespHeaders) > 0 {
			break
		}

		_, msg, err := ReadStream(consecRespListenArgs, readerLabel+"<consecHeadersCheck>")
		if err != nil {
			go LoggerFunc(err.Error())
			retErr = err
			break
		}
		if str, ok := msg.([]redis.XStream); ok {
			interConsecRespHeaders = FilterMsgVals(str[0].Messages[0], func(k, v string) bool {
				return k == "CONSEC_RESP"
			})
		} else {
			go LoggerFunc(fmt.Sprintf("%v - Consec resp msg type assertion error", args["botStream"]))
			retErr = err
			break
		}

		//convert output interface{} to []string{}
		if conv, ok := interConsecRespHeaders.(string); ok {
			consecRespHeaders = strings.Split(conv, ",")
		} else {
			return nil, fmt.Errorf("fail to convert consecutive response header field with value \"%s\"", interConsecRespHeaders)
		}
	}

	fmt.Printf("Got consec resp headers to wait for = %v\n", consecRespHeaders)

	//listen for consecutive responses with headers
	receivedHeaders := map[string]bool{}
	for _, hd := range consecRespHeaders {
		receivedHeaders[hd] = false
	}
	for {
		LoggerFunc(fmt.Sprintf("Listening new consec header | all = %v | %v\n", receivedHeaders, args))

		//stop listening if all consec headers received
		allReceived := true
		for _, h := range receivedHeaders {
			if !h {
				allReceived = false
				break
			}
		}
		if allReceived {
			break
		}

		var headerVal string
		_, msg, err := ReadStream(consecRespListenArgs, readerLabel+"<consecHeaderWait>")
		if err != nil {
			go LoggerFunc(err.Error())
			retErr = err
			break
		}
		if str, ok := msg.([]redis.XStream); ok {
			headerVal = FilterMsgVals(str[0].Messages[0], func(k, v string) bool {
				LoggerFunc(fmt.Sprintf("Read consec header %v", v))
				return k == "CONSEC_HEADER"
			})
		} else {
			go LoggerFunc(fmt.Sprintf("%v - Consec resp msg type assertion error", args["botStream"]))
			retErr = err
			break
		}

		for k := range receivedHeaders {
			if k == headerVal {
				receivedHeaders[k] = true
			}
		}

		LoggerFunc(fmt.Sprintf("Received consec header %v | all = %v | %v\n", headerVal, receivedHeaders, args))
	}

	return nil, retErr
}

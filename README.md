# Streams Message Broker

Event-driven microservices using Redis streams.

## Usage
```
// init 

msngr.GoogleProjectID = "gcp-project"
msngr.InitDatastore()
//reads REDISHOST and REDISPORT env vars
msngr.InitRedis()

// start using pkg

msngr.ListenStream(...)
```

## Local Dev

```
chmod +x run.sh
./run.sh
```

Inside `run.sh`:
```
# build/ directory ignored by git
go build -o build/api .

build/api
```

## Publishing

To reset pkg.dev.go cache so that `go get` works on repos that use it:
```
git tag vX.X.X
git push --tags
curl https://sum.golang.org/lookup/gitlab.com/myikaco/msngr@vX.X.X
```
module gitlab.com/myikaco/msngr

go 1.16

require (
	cloud.google.com/go/datastore v1.5.0
	github.com/go-redis/redis/v8 v8.5.0
)

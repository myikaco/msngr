package msngr

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	"cloud.google.com/go/datastore"
	"github.com/go-redis/redis/v8"
)

func AddToStream(streamName string, msgs []string) {
	var ctxStrat = context.Background()

	newID, err := rdb.XAdd(ctxStrat, &redis.XAddArgs{
		Stream: streamName,
		Values: msgs,
		ID:     "*",
	}).Result()
	if err != nil {
		LoggerFunc(fmt.Sprintf("XADD error = %v", err.Error()))
	}

	if newID != "" {
		LoggerFunc(fmt.Sprintf("Added %s to stream %s | %v", newID, streamName, msgs))
	}
}

// ReadStream returns (<last message id> string, <messages in <streamName> from <startID> onwards> in format []redis.XStream).
//
// Required string keys for args map: streamName, groupName, consumerName, start, count
//
// If <groupName> and <consumerName> are "", will use XREAD. Otherwise, will use XREADGROUP.
//
// If using XREADGROUP, startID can be ">" to get new messages or "0" to get message history of consumer.
func ReadStream(args map[string]string, label string) (interface{}, interface{}, error) {
	// LoggerFunc(fmt.Sprintf("Entering ReadStream (%v) with args = %v", label, args))
	if rdb == nil {
		return "", nil, &RedisClientError{}
	}

	var ret []redis.XStream = []redis.XStream{}
	var retLastID string = ""
	var ctx = context.Background()
	//diff action depending on params
	var res []redis.XStream
	var err error
	c, _ := strconv.Atoi(args["count"])
	if args["consumerName"] == "" && args["groupName"] == "" {
		rArgs := redis.XReadArgs{
			Streams: []string{args["streamName"], args["start"]},
			Block:   time.Duration(0),
			Count:   int64(c),
		}
		LoggerFunc(fmt.Sprintf("Run XREAD (%v), args = %v", label, args))
		res, err = rdb.XRead(ctx, &rArgs).Result()
	} else {
		rArgs := redis.XReadGroupArgs{
			Group:    args["groupName"],
			Consumer: args["consumerName"],
			Streams:  []string{args["streamName"], ">"},
			Count:    int64(c),
			Block:    time.Duration(0),
		}
		LoggerFunc(fmt.Sprintf("Run XREADGROUP (%v), args = %v", label, args))
		res, err = rdb.XReadGroup(ctx, &rArgs).Result()
	}

	//return vals if no err
	if err != nil && err.Error() != "redis: nil" {
		LoggerFunc(fmt.Sprintf("ReadStream err = %v", err.Error()))
		// fmt.Println("Sleeping 5s before retry...")
		time.Sleep(5000 * time.Millisecond)
	} else {
		//check res len and messages len before returning
		if len(res) > 0 && len(res[len(res)-1].Messages) > 0 {
			ret = res
			retLastID = res[len(res)-1].Messages[0].ID
		}
	}

	return retLastID, ret, err
}

// GetOwnPendingMsgs gets all pending (unacknowledged) messages assigned to the consumer, consumerName.
func GetOwnPendingMsgs(streamName, groupName, consumerName string, count int64) []redis.XStream {
	ctx = context.Background()
	ret, err := rdb.XReadGroup(ctx, &redis.XReadGroupArgs{
		Group:    groupName,
		Consumer: consumerName,
		Streams:  []string{streamName, "0"},
		Count:    count,
	}).Result()

	if err != nil {
		LoggerFunc(err.Error())
	}

	return ret
}

// GetGroupPendingMsgs gets all the pending (unacknowledged) messages in the stream consumer group.
//
// Pass "" as the consumer argument to get pending messages for all consumers
func GetGroupPendingMsgs(streamName, groupName, consumer string, count int64) []redis.XPendingExt {
	ctx = context.Background()
	ret, err := rdb.XPendingExt(ctx, &redis.XPendingExtArgs{
		Stream:   streamName,
		Group:    groupName,
		Start:    "-",
		End:      "+",
		Count:    count,
		Consumer: consumer,
	}).Result()

	if err != nil {
		LoggerFunc(err.Error())
	}

	return ret
}

func AcknowledgeMsg(streamName, groupName, consumer, id string) {
	ctx = context.Background()
	_, err := rdb.XAck(ctx, streamName, groupName, id).Result()

	if err != nil {
		LoggerFunc(err.Error())
	}
}

type RedisClientError struct{}

func (m *RedisClientError) Error() string {
	return "package msngr cannot connect to redis client"
}

// AutoClaimPendingMsgs automatically claims <count> pending(unacknowledged) message(s).
//
// Required string keys for args map: streamName, groupName, consumerName, start, count, minIdleTime
//
// Returns ([]redis.XStream, cmdError, redisClientErr).
//
// <start> min value is "0-0", but can also be a stream ID (used as a cursor).
//
// Claimed message(s) are returned.
func AutoClaimPendingMsgs(args map[string]string, label string) (interface{}, interface{}, error) {
	if rdb == nil {
		return "", nil, &RedisClientError{}
	}

	// LoggerFunc(fmt.Sprintf("Run XAUTOCLAIM (%v), args = %v", label, args))
	ctx = context.Background()
	rawRet, cmdErr := rdb.Do(
		ctx,
		"XAUTOCLAIM",
		args["streamName"],
		args["groupName"],
		args["consumerName"],
		args["minIdleTime"],
		args["start"],
		"COUNT",
		args["count"]).Result()
	if cmdErr != nil {
		LoggerFunc(cmdErr.Error())
	}

	var retMsgs []redis.XMessage
	if rawRet == nil {
		return nil, nil, nil
	}

	//convert return type to []redis.XStream
	for _, s := range rawRet.([]interface{}) {
		//convert to redis.XStream
		if id, ok := s.(string); ok {
			retMsgs = append(retMsgs, redis.XMessage{
				ID: id,
			})
		}
		if r, ok := s.([]interface{}); ok {
			if len(r) > 0 && len(r[0].([]interface{})) > 0 {
				//parse values array
				var allVals = make(map[string]interface{})
				msgValsArray := r[0].([]interface{})[1].([]interface{})
				for j, val := range msgValsArray {
					if j%2 == 0 {
						allVals[val.(string)] = msgValsArray[j+1]
					}
				}

				if allVals["TradeStreamName"] != nil {
					retMsgs = append(retMsgs, redis.XMessage{
						ID:     r[0].([]interface{})[0].(string),
						Values: allVals,
					})
				}
			}
		}
	}
	retFinal := []redis.XStream{
		{
			Stream:   args["streamName"],
			Messages: retMsgs,
		},
	}

	return retFinal, cmdErr, nil
}

func SaveMsg(streamMsg []string) {
	//build string to store
	var data string
	for _, r := range streamMsg {
		data = data + r + ","
	}

	ctx := context.Background()
	client, err := datastore.NewClient(ctx, GoogleProjectID)
	if err != nil {
		log.Fatalf("Failed to create client: %v", err)
	}
	kind := "TradeAction"
	newBotKey := datastore.IncompleteKey(kind, nil)
	actionName := FindInStreamMsg(streamMsg, "HEADER")
	aggrID, _ := strconv.Atoi(FindInStreamMsg(streamMsg, "AGGR_ID"))
	sz, _ := strconv.ParseFloat(FindInStreamMsg(streamMsg, "SIZE"), 32)

	//TODO: replace with package to import types later
	type TradeAction struct {
		KEY         string  `json:"KEY,omitempty"`
		UserID      string  `json:"UserID,omitempty"`
		Action      string  `json:"Action"`
		AggregateID int     `json:"AggregateID,string"`
		BotID       string  `json:"BotID"`
		Size        float32 `json:"Size"`
		TimeStamp   string  `json:"TimeStamp"`
		Ticker      string  `json:"Ticker"`
		Exchange    string  `json:"Exchange"`
	}
	newAction := TradeAction{
		Action:      actionName,
		AggregateID: aggrID,
		BotID:       "", //TODO: fill later
		Size:        float32(sz),
		TimeStamp:   time.Now().Format("2006-01-02_15:04:05_-0700"),
		UserID:      "", //TODO: fill later
	}

	if _, err := client.Put(ctx, newBotKey, &newAction); err != nil {
		log.Fatalf("Failed to save TradeAction: %v", err)
	}
}

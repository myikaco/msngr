package msngr

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v8"
)

func InitRedis(host, port, pass string) {
	// default to dev redis instance
	if host == "" {
		host = "127.0.0.1"
	}
	if port == "" {
		port = "6379"
	}
	LoggerFunc(fmt.Sprintf("msngr connect Redis " + host + ":" + port))

	rdb = redis.NewClient(&redis.Options{
		Addr:        host + ":" + port,
		Password:    pass,
		IdleTimeout: -1,
	})
	ctx := context.Background()
	rdb.Do(ctx, "AUTH", pass)
}

func PingLoop() {

}

func CreateNewConsumerGroup(streamName, groupName, startID string) (string, error) {
	var ctx = context.Background()
	res, err := rdb.XGroupCreateMkStream(ctx, streamName, groupName, startID).Result()

	return res, err
}

var chars = []rune("1234567890abcdefghijklmnopqrstuvwxyz")

func generateRandomID(length int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	b := make([]rune, length)
	for i := range b {
		b[i] = chars[rand.Intn(len(chars))]
	}
	return string(b)
}

func GenerateNewConsumerID(prefix string) string {
	u := fmt.Sprint(time.Now().Unix())
	return prefix + "_" + u + "_" + generateRandomID(20)
}

func SaveLastID(saveKey string, idToSave string) error {
	var ctx = context.Background()
	err := rdb.Set(ctx, saveKey, idToSave, 0).Err()
	return err
}

//TODO: modify this function to take a lambda that executes on every key/value in msg
func FindInStreamMsg(streamResp []string, key string) string {
	var data string
	readKey := false
	for _, r := range streamResp {
		if r == key {
			readKey = true
		} else if readKey {
			readKey = false
			data = r
		}
	}
	return data
}

// FilterMsg returns the value of a key in a redis.XMessage Values map.
// Param 1, <msg>, is the redis.XMessage to find the value in.
// Param 2, <filterer>, is a function which takes a key in the Values map and its corresponding value. This function should return <true> if the value should be returned.
func FilterMsgVals(msg redis.XMessage, filterer func(string, string) bool) string {
	ret := ""
	for key, val := range msg.Values {
		if filterer(key, val.(string)) {
			ret = val.(string)
		}
	}
	return ret
}

package msngr

import (
	"context"

	"github.com/go-redis/redis/v8"
)

// types

type StreamMsg struct {
	MsgID   string
	MsgVals map[string]string
}

// vars

var GoogleProjectID string
var rdb *redis.Client
var ctx context.Context

var LoggerFunc func(string)

var colorReset = "\033[0m"
var colorRed = "\033[31m"
var colorGreen = "\033[32m"
var colorYellow = "\033[33m"
var colorBlue = "\033[34m"
var colorPurple = "\033[35m"
var colorCyan = "\033[36m"
var colorWhite = "\033[37m"
